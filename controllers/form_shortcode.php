<?php

class FormShortCode {

    static function fb_add_tinymce() {
        global $typenow;

        // only on Post Type: post and page
        if (!in_array($typenow, array('post', 'page')))
            return;

        add_filter('mce_external_plugins', array(get_called_class(), 'fb_add_tinymce_plugin'));
        // Add to line 1 form WP TinyMCE
        add_filter('mce_buttons', array(get_called_class(), 'fb_add_tinymce_button'));
    }

// inlcude the js for tinymce
    static function fb_add_tinymce_plugin($plugin_array) {

        $plugin_array['fb_test'] = plugins_url('../views/js/plugin.js', __FILE__);
        // Print all plugin js path
        //var_dump( $plugin_array );
        return $plugin_array;
    }

    // Add the button key for address via JS
    static function fb_add_tinymce_button($buttons) {

        array_push($buttons, 'fb_test_button_key');
        // Print all buttons
        // var_dump( $buttons );
        return $buttons;
    }

    //////////////////////////////////draw short code ////////////////////////////
    static function draw_form($att) {
        global $wpdb;
        $form_id = $att['id'];
        $form = Form::find_by_id($form_id);
        $user_options = json_decode($form->user_options);
        $user_options = $user_options->redirct_url;
        //print_r($user_options->url);
        echo "<form  class='$form->class' style='direction:$form->direction' method={$user_options->method} action={$user_options->url}>";
        $elements = FormShortCode::draw_elements($form_id);
        echo $elements;
        echo "</form>";
    }

    //
    static function draw_elements($form_id) {
        //echo $form_id;
        $form = Form::find_by_id($form_id);
        $elements = json_decode($form->elements);
        if (!empty($elements)) {
            echo "<input type='hidden' value='" . $form_id . "' name='form_id'>";
            echo "<table class='div-element'>";

            foreach ($elements as $key => $value) {
                echo "<tr>";
                if ($value->tag != 'tag' && isset($value->Label))
                    echo "<th><label>" . $value->Label . "</label></th>";
                switch ($value->tag) {
                    case 'input':
                        if ($value->type == 'radio') {
                            foreach ($value->Options as $k => $v) {
                                ?>
                                <td><label><?php echo $k; ?></label>
                                    <input type="<?php echo $value->type; ?>" class="<?php if (isset($value->class)) echo $value->class; ?>"  placeholder="<?php echo $value->Placeholder; ?>"  name="<?php if (isset($value->name)) echo str_replace(' ', '_', $value->name); ?>" value="<?php echo $v; ?>"/></td>

                                <?php
                            }
                        } else {
                            ?>
                            <td><input type="<?php echo $value->type; ?>" class="<?php if (isset($value->class)) echo $value->class; ?>" placeholder="<?php if (isset($value->Placeholder)) echo $value->Placeholder; ?>" name="<?php if (isset($value->name)) echo str_replace(' ', '_', $value->name); ?>" 
                                       value="<?php echo $value->Value; ?>" /></td>
                                <?php
                            }
                            break;

                        case 'textarea':
                            ?>
                        <td><textarea class="<?php if (isset($value->class)) echo $value->class; ?>" name="<?php echo $value->Name; ?>" name="<?php if (isset($value->name)) echo str_replace(' ', '_', $value->name); ?>"></textarea></td>
                        <?php
                        break;

                    case 'select':
                        ?>
                        <td><select class="<?php if (isset($value->class)) echo $value->class; ?>" name="<?php if (isset($value->name)) echo str_replace(' ', '_', $value->name); ?>" >
                                <?php foreach ($value->Options as $select_k => $select_v) { ?>
                                    <option value="<?php echo $select_v; ?>"><?php echo $select_k; ?></option>

                                <?php } ?>
                            </select></td>

                        <?php
                        break;

                    case 'captacha':
                        echo '<td><div class="controls">
                            <label class="" for="captcha">*Please enter the verication code shown below.</label>
                            <div id="captcha-wrap" >
                                <img src="' . plugins_url('mnbaa_namozagk/views/images/img/refresh.jpg') . '" alt="refresh captcha" id="refresh-captcha" /> 
                                <img src="' . plugins_url('mnbaa_namozagk/helpers/newCaptcha.php') . '" alt="" id="captcha" />
                            </div>
                            <input class="narrow text input" id="captcha" name="captcha" type="text" placeholder="Verification Code">
                        </div></td>';

                        break;

                    default:
                        ?>

                        <<?php echo $value->tagName; ?>  class="<?php if (isset($value->class)) echo $value->class; ?>" style='font-size: <?php echo $value->fontSize; ?>'><?php echo $value->Text; ?> </<?php echo $value->tagName; ?> >
                        <?php
                        break;
                }
            }
            echo "</tr>";
        }

        //FormController::draw_element($value);

        echo "</table>";
    }

//
}
?>
