<?php

class FormController {

    static function add_form_page() {
        add_menu_page('Mnbaa Namozagk', __('Mnbaa Namozagk', 'namozaghk'), 'manage_options', 'namozagk', array(get_called_class(), 'check_actions'));
    }

    static function add_sub_menu_page() {

        add_submenu_page('namozagk', 'Mnbaa Namozagk options', __('Namozagk options', 'namozaghk'), 'manage_options', 'options', array(get_called_class(), 'view_options')
        );
    }

    //
    static function view_options() {
        if (isset($_POST['records_num_forms'])) {
            // echo "here";
            $form_records_num = $_POST['records_num_forms'];
            update_option('records_num_forms', $form_records_num);
            //
            $data_records_num = $_POST['records_num_data'];
            update_option('records_num_formdata', $data_records_num);
        }
        include( plugin_dir_path(__FILE__) . '../views/pagintion_options.php');
    }

    //
    static function get_all_for_shortcode() {
        global $wpdb;
        $results = Form::find_all();
        $count = 0;
        // echo"footer";
        echo '<script type="text/javascript">
	        var all_forms = new Array();';
        echo "var values = [";
        if (isset($results) && !empty($results)) {
            foreach ($results as $key => $value) {

                echo "{text: '{$value->name}', value: '{$value->id}'},";
            }
        } else
            echo "{text: 'no forms', value: '0'},";
        echo "];";
        echo'</script>';
    }

    //
    static function check_actions() {
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
            if (isset($_GET['id']))
                $id = $_GET['id'];
            if ($action === "add" || $action === "edit") {
                //echo "yes";
                include( plugin_dir_path(__FILE__) . '../views/new_form.php');
            } elseif ($action == 'delete') {
                Form::delete($id);
                FormController:: view_all_forms();
            } elseif ($action == 'elements') {
                FormController:: view_form_data();
                // include( plugin_dir_path(__FILE__) . '../views/elements_by_form.php');
            } elseif ($action == 'search') {
                $form_id = $_GET['id'];
                FormController:: search_form_data($form_id);
            }
        } else
            FormController:: view_all_forms();
    }

    static function search_form_data($form_id) {
        $elements = Element::find_by_field("form_id", $form_id);
        $names = json_decode($elements[0]->data);
        $search_query = $_POST['search_query'];
        echo "<h2>" . __('Search Result', 'namozaghk') . "</h2>";
        echo "<table class ='widefat'><thead><tr>";
        foreach ($names as $v) {
            echo "<th>" . $v->name . "</th>";
        }
        echo "</tr></thead>";
        foreach ($elements as $key => $value) {
            $data = json_decode($value->data);
            foreach ($data as $k => $v) {
                $query = $v->value;
                if (strpos($query, $search_query) !== false) {
                    echo "<tr>";
                    foreach ($data as $k => $v) {
                        echo "<td>" . $v->value . "</td>";
                    }
                    echo"</tr>";
                }
                break;
            }
        }
        echo "</table>";
    }

    static function view_all_forms() {
        $pagenum = isset($_GET['paged']) ? absint($_GET['paged']) : 1;
        $records_num = get_option('records_num_forms', 10);
        (!empty($records_num)) ? $limit = $records_num : $limit = 10;
        $offset = ( $pagenum - 1 ) * $limit;
        $total = FORM::find_count();
        $num_of_pages = ceil($total / $limit);
        $entries = Form::find_limited($limit, $offset);
        $page_links = paginate_links(array(
            'base' => add_query_arg('paged', '%#%'),
            'format' => '',
            'prev_text' => __('&laquo;', 'text-domain'),
            'next_text' => __('&raquo;', 'text-domain'),
            'total' => $num_of_pages,
            'current' => $pagenum
        ));

        include( plugin_dir_path(__FILE__) . '../views/all_forms.php');
    }

    function json_decode_js2php($json_str) {
        $x = str_replace('\\', '', $json_str);
        return json_decode($x);
    }

    static function add_form() {
        //echo "here";
        $id = $_POST['form_id'];
        if (isset($_POST['atributes'])) {
            $from_name = $_POST['atributes']['name'];
            if (isset($_POST['atributes']['elements'])) {
                $form_elements = $_POST['atributes']['elements'];
            }
        }
        //echo  $_POST['atributes']['user_options'];
        $_POST['atributes']['user_options'] = str_replace('\\', '', $_POST['atributes']['user_options']);
        if (!empty($from_name)) {
            if (!empty($form_elements)) {
                //echo "not empry";
                //claen values from slash json
                foreach ($form_elements as $k => $element) {
                    $clean_element = str_replace('\\', '', $element);
                    $_POST['atributes']['elements'][$k] = json_decode($clean_element);
                }

                $_POST['atributes']['elements'] = json_encode($_POST['atributes']['elements']);
                //
            } else
                $_POST['atributes']['elements'] = "";
            $post = $_POST['atributes'];
            //var_dump($post);

            if (isset($id) && $id != '') {
                $form = Form::find_by_id($id);
                //check if update in elemnts not on properties of form
                if ($form->elements != $_POST['atributes']['elements']) {
                    Element::delete_by_field('form_id', $id);
                }
                Form::update($post, $id);
            } else {

                if (isset($_POST['atributes']) && $_POST['atributes'] != '') {
                    Form::create($post);
                }
            }
            wp_redirect(admin_url() . "/admin.php?page=namozagk");
        } else
            wp_redirect(admin_url() . "/admin.php?page=namozagk&action=add&save_msg='failed'");
    }

//
    static function draw_ajax_ele() {
        $value = $_POST['json_data'];
        FormController::draw_element($value);
        die();
    }

    /**
     * function to add style of form from user to custom css file
     * 
     */
    static function add_form_style() {

        // echo $full_path;
        $content = $_POST['form_style'];
        $form_name = $_POST['form_name'];
        //
        //die($form_name);
        $full_path = plugin_dir_path(__DIR__) . 'views/styles/'.$form_name.'_style.css';
        $access_type = get_filesystem_method();
        if ($access_type == 'direct') {

            /* you can safely run request_filesystem_credentials() without any issues and don't need to worry about passing in a URL */
            $creds = request_filesystem_credentials(site_url() . '/wp-admin/', '', false, false, array());

            /* initialize the API */
            if (!WP_Filesystem($creds)) {
                /* any problems and we exit */
                return false;
            }

            global $wp_filesystem;
            if (!$wp_filesystem->put_contents($full_path, $content, 0777)) {
                die('Error saving file!');
            } else {
                die("sucessed");
            }
            /* do our file manipulations below */
        } else {
            echo "failed";
            die();
        }
    }

//
    function post_data() {

        $post = $_POST['json_data'];
        $form_id = $_POST['form_id'];
        $ele = array_shift($post);
        $data = json_encode($post);
        global $wpdb;

        $wpdb->insert(
                $wpdb->prefix . 'mnbaa_elements', array(
            'form_id' => $form_id,
            'data' => $data,
                ), array(
            '%d',
            '%s'
                )
        );
        $element_id = $wpdb->insert_id;
        //echo $element_id;
        $form = FORM::find_by_id($form_id);
        $elemnets = json_decode($form->elements);
        $user_options = json_decode($form->user_options);
        $mail_content = "";

        if ($user_options->send_email != '') {
            $mail_content.= '<html><body>';
            $mail_content.= "<p>Form Name:" . $form->name . "</p>";
            $mail_content.= '<table rules="all" style="border-color: #666;" cellpadding="10"><thead><tr style="background: #eee;">';
            $all_elements = Element::find_by_id($element_id);
            $data = json_decode($all_elements->data);
            foreach ($data as $k => $v) {
                $mail_content.="<td>" . $v->name . "</td>";
            }
            $mail_content.= "</tr><tr>";
            foreach ($data as $k => $v) {
                $mail_content.="<td>" . $v->value . "</td>";
            }
            $mail_content.="</tr></table>";
            $mail_content.= "</body></html>";
            // sent mail data
            $to = $user_options->send_email;
            //$headers = "From: " . strip_tags($_POST['req-email']) . "\r\n";
            //$headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
            $headers = "CC: mnbaa@example.com\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            // $headers = 'From:'.get_bloginfo('name').'-Mnbaa Namozaghk';
            //$blog_title = get_bloginfo('name');
            $subject = $form->name . 'data';
            wp_mail($to, $subject, $mail_content, $headers);
        }
        //echo $mail_content;
        $url_options = $user_options->redirct_url;
        echo $url_options->url;
        session_start();
        if (isset($_REQUEST['code'])) {
            //echo strtolower($_SESSION['captcha']);
            echo '*' . json_encode(strtolower($_REQUEST['code']) == strtolower($_SESSION['captcha']));
        }

        die();
    }

    static function view_form_data() {
        $form_id = $_GET['id'];
        $form = Form::find_by_id($form_id);
        $headers = json_decode($form->elements);
        echo "<h2>" . $form->name . " Elements data</h2>";
        $all_elements = Element::find_by_field('form_id', $form_id);
        //paagination code
        $pagenum = isset($_GET['paged']) ? absint($_GET['paged']) : 1;
        $records_num = get_option('records_num_formdata', 10);
        (!empty($records_num)) ? $elements_limit = $records_num : $elements_limit = 10;
        //$elements_limit = 10; // number of rows in page
        $offset = ( $pagenum - 1 ) * $elements_limit;
        $total = count($all_elements);
        $elements_entry = Element::find_by_field_limited('form_id', $form_id, $elements_limit, $offset);
        $num_of_pages = ceil($total / $elements_limit);
        //$entries=Element::find_limited($limit, $offset);
        $page_links = paginate_links(array(
            'base' => add_query_arg('paged', '%#%'),
            'format' => '&paged=%#%',
            'prev_text' => __('&laquo;', 'text-domain'),
            'next_text' => __('&raquo;', 'text-domain'),
            'total' => $num_of_pages,
            'current' => $pagenum
        ));
        include( plugin_dir_path(__FILE__) . '../views/elements_by_form.php');
    }

    //

    static function draw_element($value) {

        $value = (array) $value;
        echo '<label class="draged_label">';
        if (isset($value["Label"])) {
            _e($value["Label"], 'namozaghk');
        }
        echo '</label>';
        switch ($value['tag']) {
            case 'input':
                if ($value['type'] == 'radio') {
                    ?>
                    <div  class="all-option">
                    <?php
                    foreach ($value['Options'] as $k => $v) {
                        ?>
                            <div >
                                <label ><?php echo $k; ?></label>
                                <input type="<?php echo $value['type']; ?>"  value="<?php echo $v; ?>" disabled=""/>
                            </div>
                        <?php
                    }
                    ?>
                    </div>
                    <?php } else { ?>
                    <input type="<?php echo $value['type']; ?>"  <?php if ($value['type'] != 'checkbox' && $value['type'] != 'submit' && $value['type'] != 'color') { ?> placeholder="<?php echo $value['Placeholder']; ?>" <?php } ?>
                    <?php if (isset($value['Value'])) { ?> value="<?php echo $value['Value']; ?>" <?php } ?> readonly=""  disabled=""  <?php if (isset($value['Label'])) { ?>name="<?php echo str_replace(' ', '_', $value['Label']); ?>" <?php } ?>/>

                           <?php
                       }
                       break;

                   case 'textarea':
                       echo "<textarea readonly='' ></textarea>";
                       break;

                   case 'tag':
                       ?>
                <<?php echo $value['tagName']; ?>><?php echo $value['Text']; ?>
                </<?php echo $value['tagName']; ?>
                >
                <?php
                break;

            case 'select':
                echo '<select disabled="">';
                foreach ($value['Options'] as $select_k => $select_v) {
                    ?>
                    <option value="<?php echo $select_v; ?>"><?php echo $select_k; ?><option>

                <?php } ?>
                </select>

                <?php
                break;

            case 'captacha':
                echo '<div class="controls">
                        <label class="" for="captcha">*Please enter the verication code shown below.</label>
                        <div id="captcha-wrap">
                            <img src="' . plugins_url('mnbaa_namozagk/views/images/img/refresh.jpg') . '" alt="refresh captcha" id="refresh-captcha" /> 
                            <img src="' . plugins_url('mnbaa_namozagk/helpers/newCaptcha.php') . '" alt="" id="captcha" />
                        </div>
                        <input class="narrow text input" id="captcha" name="captcha" type="text"   readonly=""  disabled="" placeholder="Verification Code">
                    </div>';


            default:
                break;
        }
    }

}
?>
