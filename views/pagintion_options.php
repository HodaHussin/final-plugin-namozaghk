<h2><?php _e('Number of records per page ','namozaghk');?></h2>
<form method="post" action="">
	<table class="form-table ">
	    <tr>
            <td><?php Mnbaa_Namozagk_label(__('Record no for forms','namozaghk'), "") ?></td>
            <td><?php Mnbaa_Namozagk_input('records_num_forms',get_option('records_num_forms', '' )) ?></td>
        </tr>
		<tr>
			<td><?php Mnbaa_Namozagk_label(__('Record no for form data','namozaghk'), "") ?></td>
			<td><?php Mnbaa_Namozagk_input('records_num_data',get_option('records_num_formdata', '' )) ?></td>
		</tr>
		
	</table>
	<input type="submit"  value="<?php _e('Save','namozaghk')?>" class="button button-primary" />
</form>

