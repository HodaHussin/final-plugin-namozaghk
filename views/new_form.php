<?php
if (isset($_GET['save_msg'])) {
    $msg = $_GET['save_msg'];
    if ($msg == 'failed') {
        echo "<div class='error'><p>" . __('You Should enter form name', 'namozaghk') . "</p></div>";
    }
}
?>
<!-- <h1>New Form</h1> -->
<div id="form_creator">
    <div id="basic-form">
        <form method="post" class="namozagk-form" action="<?php echo admin_url(); ?>admin-post.php?action=add_namozagk_form">
            <table class="form-table ">
                <tr class="form-tr">
                    <td>
                        <?php Mnbaa_Namozagk_label(__('Form name', 'namozaghk'), "") ?>
                    </td>
                    <td colspan="2">
                        <?php
                        global $elements, $user_options;
                        if (isset($_GET['id'])) {
                            $id = $_GET['id'];
                            $form = Form::find_by_id($id);
                            Mnbaa_Namozagk_input("atributes[name]", $form->name, 'form-name');
                            ?>
                        </td>
                    </tr>
                    <tr><td >
                            <?php Mnbaa_Namozagk_label(__('Class Name', 'namozaghk'), "") ?>
                        </td>
                        <td><?php Mnbaa_Namozagk_input("atributes[class]", $form->class, 'form-class'); ?></td>
                    </tr>
                    <tr>
                        <td >
                            <?php Mnbaa_Namozagk_label(__('Form Direction', 'namozaghk'), "") ?>
                        </td>
                        <td><?php
                            if ($form->direction == 'lrt') {
                                Mnbaa_Namozagk_radio('atributes[direction]', __('lrt', 'namozaghk'), 'lrt', 'checked="checked"');
                                echo "</td><td>";
                                Mnbaa_Namozagk_radio('atributes[direction]', __('rtl', 'namozaghk'), 'rtl', '');
                            } else {
                                Mnbaa_Namozagk_radio('atributes[direction]', __('lrt', 'namozaghk'), 'lrt', '');
                                echo "</td><td>";
                                Mnbaa_Namozagk_radio('atributes[direction]', __('rtl', 'namozaghk'), 'rtl', 'checked="checked"');
                            }
                            ?></td>
                    </tr>

                    <?php
                } else {
                    Mnbaa_Namozagk_input("atributes[name]", '', 'form-name');
                    ?>
                    <tr><td >
                            <?php Mnbaa_Namozagk_label(__('Class Name', 'namozaghk'), "") ?>
                        </td>
                        <td><?php Mnbaa_Namozagk_input("atributes[class]", '', 'form-class'); ?></td>
                    </tr>
                    <tr>
                        <td >
                            <?php Mnbaa_Namozagk_label(__('Form Direction', 'namozaghk'), "") ?>
                        </td>
                        <td><?php Mnbaa_Namozagk_radio('atributes[direction]', 'lrt', 'lrt', 'checked="checked"'); ?></td>
                        <td><?php Mnbaa_Namozagk_radio('atributes[direction]', 'rtl', 'rtl', ''); ?></td>
                    </tr>

                <?php } ?>
                <tr>
                <input type="hidden" name="form_id"  value="<?php if (isset($_GET['id'])) echo $_GET['id']; ?>"/>
                <input type="hidden" name="atributes[user_id]"  value="<?php echo get_current_user_id(); ?>"/>
                <input type="hidden" name="atributes[time]"  value="<?php echo current_time('mysql'); ?>"/> 
                <input type="hidden" name="atributes[user_options]"  value='<?php
                if (isset($_GET['id'])) {
                    $form = Form::find_by_id($_GET['id']);
                    echo $form->user_options;
                } else
                    echo json_encode($user_options);
                ?>' class='user_options' />

                </tr>
            </table>

            <div class="drag_items">
                <h3 class="drag_here_title"><?php _e('Drag Fields Here', 'namozaghk'); ?></h3>


                <div id="dragged-elements" class="ui-widget-content">

                    <?php
                    if ($_GET['action'] == 'edit') {
                        $form_id = $_GET['id'];
                        //
                        //FormShortCode::draw_elements($form_id);
                        $form = Form::find_by_id($form_id);
                        if (trim($form->elements)) {
                            $json_elements = json_decode($form->elements);
                            //var_dump($elements);
                            ?>
                            <input type="hidden" id="ele_count" value="<?php echo count((array) $json_elements); ?>" />  
                            <?php
                            $ele_key = 0;
                            if (!empty($json_elements))
                                foreach ($json_elements as $key => $value) {
                                    ?>
                                    <li class="ui-state-default ui-draggable dragged" type-id="<?php echo $value->type . $ele_key; ?>">
                                        <div class="basic">
                                            <?php FormController::draw_element($value); ?>
                                        </div>

                                        <div class="actions">
                                            <span class="form-edit"><a href="#prop"><?php _e("edit", "namozaghk"); ?></a></span>
                                            <span class="form-close">Delete</span>
                                        </div> 
                                        <input type="hidden" name="atributes[elements][<?php echo $ele_key; ?>]"  value='<?php echo json_encode($value); ?>'  class ="json_val" id="<?php echo $key; ?>_ele"/>
                                    </li>
                                    <?php
                                    $ele_key++;
                                }
                        }
                    }
                    ?>

                </div>
            </div>

            <input type="submit" value ="<?php _e('Save', 'namozaghk'); ?>" >
        </form>
    </div>

    <div id="setting">

        <h2 class="nav-tab-wrapper" id="wpseo-tabs" style="padding:0px;">
            <ul class="category-tabs" style="margin:0px;padding:0px;">
                <li>
                    <a href="#tabs-1" ><?php echo __('Elements', 'namozaghk'); ?></a>
                </li>
                <li>
                    <a href="#tabs-2"><?php echo __('Field Setting', 'namozaghk'); ?></a>
                </li>
                <li>
                    <a href="#tabs-3"><?php echo __('After Submit', 'namozaghk'); ?></a>
                </li>
                <li>
                    <a href="#tabs-4"><?php echo __('Form Style', 'namozaghk'); ?></a>
                </li>
            </ul>
        </h2>
        <div id="tabs-1">
            <ul id="draggable" class="ui-widget-content">
                <?php
                foreach ($elements as $key => $value):
                    ?>
                    <li class="ui-state-default ui-draggable" type-id="<?php echo $key; ?>">
                        <div class="basic">
                            <?php
                            $value['type'] = $key;
                            FormController::draw_element($value);
                            ?>
                        </div>
                        <input type="hidden" name="atributes[elements]"  value='<?php echo json_encode($value); ?>'  class ="json_val" id="<?php echo $key; ?>_ele"/>
                    </li> 

                <?php endforeach ?>

            </ul>
        </div>
        <div id="tabs-2">
            <ul id="prop">
                <p><?php _e('You should choose an element to edit.', 'namozaghk'); ?></p>
                <div id="basic_prop"></div>
                <a href="javascript:void(0);" class="save_prop"><?php _e('Save', 'namozaghk'); ?></a>
            </ul>
        </div>
        <div id="tabs-3">
            <form  id="options_form">
                <table>
                    <tr>
                        <td><span><?php _e('Send notification email', 'namozaghk') ?></span></td>
                        <td>
                            <?php
                            if (isset($_GET['id'])) {
                                $form = Form::find_by_id($_GET['id']);
                                $user_options = json_decode($form->user_options);
                                if ($user_options->send_email != '') {
                                    Mnbaa_Namozagk_checkbox('send_notification', 'checked');
                                    Mnbaa_Namozagk_mail('send_mail', $user_options->send_email, '');
                                } else {
                                    Mnbaa_Namozagk_checkbox('send_notification', '');
                                    Mnbaa_Namozagk_mail('send_mail', '', 'hidden');
                                }
                            } else {
                                Mnbaa_Namozagk_checkbox('send_notification', '');
                                Mnbaa_Namozagk_mail('send_mail', '', 'hidden');
                            }
                            ?>
                        </td>
                    </tr>

                    <tr>

                        <td><span><?php _e('Redirect to', 'namozaghk') ?></span></td>
                        <td>
                            <?php
                            if (isset($_GET['id'])) {
                                $url_arr = $user_options->redirct_url;
                                if ($url_arr->url != '') {
                                    Mnbaa_Namozagk_checkbox('redirect_url', 'checked');
                                    Mnbaa_Namozagk_input("redirct_val", $url_arr->url, '');
                                    //
                                    Mnbaa_Namozagk_radio('action_method', "POST", "POST", ($url_arr->method == 'POST') ? "checked" : "");
                                    Mnbaa_Namozagk_radio('action_method', "GET", "GET", ($url_arr->method == 'GET') ? "checked" : "");
                                } else {
                                    Mnbaa_Namozagk_checkbox('redirect_url');
                                    Mnbaa_Namozagk_input("redirct_val", '', 'hidden');
                                    //
                                    Mnbaa_Namozagk_radio('action_method', "POST", "POST", "checked");
                                    Mnbaa_Namozagk_radio('action_method', "GET", "GET", "");
                                }
                            } else {
                                Mnbaa_Namozagk_radio('action_method', "POST", "POST", "checked");
                                Mnbaa_Namozagk_radio('action_method', "GET", "GET", "");
                            }
                            ?>
                        </td>
                    </tr>
                </table>
                <input id="submit_handle" type="submit" style="display: none" >
            </form>
        </div>
        <div id="tabs-4">
            <?php
            $content = '';
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $form = Form::find_by_id($id);
                if (file_exists(plugin_dir_path(__DIR__) . 'views/styles/' . $form->name . '_style.css')) {
                    $file = plugin_dir_path(__DIR__) . 'views/styles/' . $form->name . '_style.css';
                    $content = file_get_contents($file);
                }
            }
            ?>
            <div class='form-style-div'>
                <p><?php _e('Enter Your Form Style.', 'namozaghk'); ?></p>
                <?php Mnbaa_Namozagk_textarea('form_style', $content) ?>
                <a href="javascript:void(0);" class="save_style"><?php _e('Save', 'namozaghk'); ?></a>
            </div>
        </div>
    </div>
</div>









