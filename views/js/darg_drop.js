var $ = jQuery;
var count = 0;

$(function() {
    //
    if (!($("#send_notification_ID").checked)) {
        $("#send_mail_ID").attr('disabled', true);
    }
    if (!($("#redirect_url_ID").checked)) {
        $("#redirct_val_ID").attr('disabled', true);
    }
    if ($("#ele_count").val()) {

        count = $("#ele_count").val();
        //alert(count);
    }

    $("#dragged-elements").sortable({
        revert: true,
        beforeStop: function(event, ui) {
            //ui.item
            parent_id = ui.item.closest('ul').attr('id');
            if (!ui.item.hasClass('dragged')) {
                type_id_attr = ui.item.attr('type-id');

                json_hidden_id = ui.item.find('input[type="hidden"]').attr('id');
                json_hidden_name = ui.item.find('input[type="hidden"]').attr('name');
                //alert(json_hidden_name);
                ui.item.find('input[type="hidden"]').attr('id', json_hidden_id + '' + count);
                ui.item.find('input[type="hidden"]').attr('name', json_hidden_name + '[' + count + ']');
                ui.item.attr('type-id', type_id_attr + '' + count);
                ui.item.append('<div class="actions"><span class="form-edit"><a href="#prop">edit</a></span> <span class="form-close">Delete</span></div>');
                //ui.item.append('<input type="hidden" value='+_case+'  name="elements['+count+'][type]" /><input type="hidden" value="field1"  name="elements['+count+'][label]" />');
                ui.item.addClass('dragged');
                count++;

                //alert(ui.item.find('.all-option div'));
                // add class to every radio option
                ui.item.find('.all-option div').each(function(key) {
                    $(this).addClass('option' + key + count);
                });

            }
        }
    });
    $("#draggable li").draggable({
        connectToSortable: "#dragged-elements",
        helper: "clone",
        scroll: true,
        scrollSensitivity: 100
    });

    $("#dragged-elements").droppable({
        hoverClass: "ui-state-hover",
    });


    $('body').on('click', '.form-close', function() {
        $(this).closest('li').remove();
    });
    //
    $('body').on('click', '.form-edit', function() {
        jQuery("#ui-id-2").click();
        var prefix_ele = ($(this).closest('li').attr('type-id'));
        //alert(prefix_ele);
        var json = $(this).closest('li').find('.json_val').val();
        var arr = JSON.parse(json);
        var str_prop = "";
        var upper_val = "";
        //$("#basic_prop").find('#ele_prop_label').val(arr.lable);
        str_prop += "<div class='ele_container' mnbaa-id='" + prefix_ele + "'><table class='form-table prop-table'>";

        for (var key in arr) {
            //alert(key);

            var value = arr[key];
            // alert(key);
            if (typeof value == 'object') {
                str_prop += "<tr><td><a href='javascript:void(0)' class ='add_option'>+</a></td></tr>";
                var i = 0;
                for (var option_key in value) {

                    option_value = value[option_key];
                    str_prop += "<tr class='option" + i + count + " opt'><th><input type='text' value= " + option_key + "  class='option-label' /></th><td><input class='" + option_key + " option-val' type ='text' value='" + option_value + "' ></td>";
                    if (i != 0)
                        str_prop += "<td><a href='javascript:void(0)' class='remove-option'>__</a></td>";
                    str_prop += "</tr>";
                    i++;
                }
            } else {
                // capatalize_key=key.charAt(0).toUpperCase()+key.slice(1);
                //alert(upper_val);
                if (key == 'tag' || key == 'type' || key == 'tagName')
                    str_prop += "<tr class ='hidden-row'>";
                else
                    str_prop += "<tr>";
                str_prop += "<th><label>" + key + "</label></th><td><input class=" + key + " type ='text' value='" + value + "'></td></tr>";
            }
            //// alert(str_prop);
        }
        str_prop += "</table></div>";
        // alert(str_prop);
        $("#basic_prop").html(str_prop);

        $(".save_prop").css('display', 'inline-block');
        $("#prop p").hide();
        //
        //alert(arr.lable);
        if ($(this).closest('li').attr('type-id') == 'radio') {
            $("#radio_prop").show();
        }
    });
    //
    $('.option').on('keypress blur', function(e) {
        //alert("hoda");
        var code = e.keyCode || e.which;
        if (code == 13 || e.type == 'blur') {
            alert(code);
            var inputVal = $(this).val().trim();
            //alert(inputVal);
            if (inputVal.length > 1) {
                var option_num = $(this).attr('option-num');
                $('.dragged').find($('.ele_option' + option_num)).html($(this).val());
                //Enter keycode
            }
            return false;
        }

    });

    $('body').on('click', '.save_prop', function() {
        //alert("yes");
        var prop_data = {};
        var prop_data_options = {};
        this_class = $("#basic_prop").first().find('.ele_container').attr('mnbaa-id');
        //alert(this_class);
        $(".prop-table tr").each(function() {
            key = $(this).find('th label').html();
            // alert(key);
            value = $(this).find('td input').val();
            //
            if ($(this).hasClass("opt")) {
                opt_key = $(this).find('th .option-label').val();
                //alert(opt_key);
                opt_value = $(this).find('td .option-val').val();
                if (opt_key)
                    prop_data_options[opt_key] = opt_value;

            } else
                prop_data[key] = value;
            //alert( JSON.stringify(prop_data_options));
            if (JSON.stringify(prop_data_options) !== '{}')
                prop_data['Options'] = prop_data_options;

        });
        //
        var prop_json = JSON.stringify(prop_data, null, 2);
        //
        jQuery.ajax({
            type: "post",
            url: myAjax.ajaxurl,
            data: {
                action: "draw_ajax_ele",
                json_data: prop_data
            },
            success: function(response) {
                $("li[type-id='" + this_class + "']").find(".basic").html(response);

            }
        });


        $("li[type-id='" + this_class + "']").find(".json_val").val(prop_json);
        //
        $("li[type-id='" + this_class + "']").find(".draged_label").html(prop_data['Label']);
        //alert($("ul li[type-id='" + this_class + "']").find(".json_val").val());
        //alert(txt);
        for (var prop_key in prop_data['Options']) {
            // $("ul li[type-id='" + this_class + "']").find(".all-option").
            var prop_value = prop_data[prop_key];

        }
        //close this tab 
        $("#ui-id-1").click();


    });

    //add style of this form to custom css called  user_style.css
    $('body').on('click', '.save_style', function() {

        data = $("#form_style_ID").val();
        form_name = $('.form-name_class').val();
        
        jQuery.ajax({
            type: "post",
            url: myAjax.ajaxurl,
            //dataType:'json',
            data: {
                action: "add_form_style",
                form_style: data,
                form_name:form_name
            },
            success: function(response) {
                //  alert(response);

            }
        });

    });

    // add options on run time
    $('body').on('click', '.add_option', function() {
        var type_id = $("#basic_prop").find('.ele_container').attr('mnbaa-id');
        //alert($("#sortable").find('li[type-id="'+type_id+'"]').html());
        $("#sortable").find('li[type-id="' + type_id + '"] .all-option').append("<div><label>Option Name</label><input class='option value' type ='radio' value='option value' disabled=''></div>");
        // alert($(this).closest('table').html());
        $(this).closest('table').append("<tr class='option opt'><th><input type='text' value= ''  class='option-label' /></th><td><input class='option-val' type ='text' value='option value'></td><td><a href='javascript:void(0)' class='remove-option'>__</a></td></tr>");
    });

    // remove  option
    $('body').on('click', '.remove-option', function() {
        // alert($(this).closest('table').html());
        $(this).closest('tr').remove();
    });

    //
    $('.namozagk-form').submit(function(event) {

        //validation on form name
        if ($('.form-name').val() == '') {
            alert("You sholud enter form name");
            return false;
        }
        //validation on mail notification
        var user_options = $('.user_options').val();
        var obj = $.parseJSON(user_options);
        //alert(obj['redirct_url']['url']);
        //alert($('input[name=action_method]:checked').val());
        if ($("#send_notification_ID").is(':checked')) {
            if ($("#send_mail_ID").val() == '') {
                alert("Enter Email to send notification");
                return false;
            } else
                obj['send_email'] = $("#send_mail_ID").val();

            //return false;

        } else {
            obj['send_email'] = '';
        }
        user_options = JSON.stringify(obj);
        $('.user_options').val(user_options);
        //
        if ($("#redirect_url_ID").is(':checked')) {
            if ($("#redirct_val_ID").val() == '') {
                alert("Enter Url to redirect form");
                return false;
            } else {
                obj['redirct_url']['url'] = $("#redirct_val_ID").val();
                obj['redirct_url']['method'] = $('input[name=action_method]:checked').val();
            }
        } else {
            obj['redirct_url']['url'] = '';
            obj['redirct_url']['method'] = 'POST';
        }
        user_options = JSON.stringify(obj);
        $('.user_options').val(user_options);

        // to excute html5 validation email and url
        //$( "#options_form" ).submit();
        $('#submit_handle').click();
        if ($('#options_form')[0].checkValidity() == false) {

        }

    });
    //
    $("#send_notification_ID").change(function() {
        if (this.checked) {
            $("#send_mail_ID").attr('disabled', false);
        } else
            //alert($("#send_mail_ID").val());
            //if($("#send_mail_ID"))

            $("#send_mail_ID").attr('disabled', true);
        $("#send_mail_ID").val(' ');

    });
    $("#redirect_url_ID").change(function() {
        if (this.checked) {
            $("#redirct_val_ID").show();
            $("#redirct_val_ID").attr('disabled', false);
            $("input[name=action_method]").attr('disabled', false);
        } else {
            //alert("ll");
            //	$("#redirct_val_ID").hide();
            $("#redirct_val_ID").attr('disabled', true);
            $("#redirct_val_ID").val('');
            $("input[name=action_method]").attr('disabled', true);
        }
    });

});

// first char To UpperCase
function firstToUpperCase(str) {
    return str.substr(0, 1).toUpperCase() + str.substr(1);
}

///////////////////////////////////////////////////
//
//

