<?php
echo '<div class="namozagk_wrap">';


echo sprintf('<h2 ><a href="?page=%s&action=%s">' . __("Add New", 'namozaghk') . '</a></h2>', $_REQUEST['page'], 'add');
?>
<table class="widefat forms-codes">
    <thead>
        <tr>
            <th scope="col" class="manage-column column-name" style=""><?php _e('Form Name', 'namozaghk'); ?></th>
            <th scope="col" class="manage-column column-name" style=""><?php _e('Short Code', 'namozaghk'); ?></th>
            <th scope="col" class="manage-column column-name" style=""><?php _e('Date', 'namozaghk'); ?> </th>
            <th scope="col" class="manage-column column-name" style=""><?php _e('Edit', 'namozaghk'); ?></th>
            <th scope="col" class="manage-column column-name" style=""><?php _e('View Details', 'namozaghk'); ?></th>
            <th scope="col" class="manage-column column-name" style=""><?php _e('Delete', 'namozaghk'); ?></th>

        </tr>
    </thead>
    <tbody>
        <?php if ($entries) {
            $count = 1;
            $class = '';
            foreach ($entries as $entry) {
                $class = ( $count % 2 == 0 ) ? ' class="alternate"' : '';
                $elements = Element::find_by_field('form_id', $entry->id);
                ?>

                <tr<?php echo $class; ?>>
                    <td><?php echo $entry->name; ?></td>
                    <td><?php echo "[MnbaaNamozagkForm ID= " . $entry->id . "]" ?></td>
                    <td><?php echo $entry->time; ?></td>
                    <td> <div ><span ><a href="?page=<?php echo $_REQUEST['page'] ?>&action=edit&id=<?php echo $entry->id; ?>" 
                                         <?php echo (!empty($elements)) ? 'onclick="return confirm(\' Editing elements will delete your last data ?\');" ' : ''; ?>  >
                                    <?php _e('Edit', 'namozaghk'); ?></a></span></div>
                    <td> <div ><span ><a href="?page=<?php echo $_REQUEST['page'] ?>&action=elements&id=<?php echo $entry->id; ?>"><?php _e('View Details', 'namozaghk'); ?></a></span></div>
                    <td> <div ><span ><a href="?page=<?php echo $_REQUEST['page'] ?>&action=delete&id=<?php echo $entry->id; ?>"
                                         onclick="return confirm(' you want to delete?');"
                                         ><?php _e('Delete', 'namozaghk'); ?></a></span></div>
                    </td>
                </tr>

                <?php
                $count++;
            }
            ?>

        <?php } else { ?>
            <tr>
                <td colspan="2"><?php _e('No posts yet', 'namozaghk'); ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php
if ($page_links) {
    echo '<div class="forms-codes-pagination"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
}

echo '</div>';
?>