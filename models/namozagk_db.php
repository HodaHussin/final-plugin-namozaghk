<?php
/**
 * DB main class
 * contains all DB functions
 */

class Mnbaa_Namozagk_DB {

	protected static $table_name;
	protected static $active_status_condition;
	protected static $db_fields;

	function __construct() {
	}

	public static function find_all() {
		global $wpdb;
		return $wpdb -> get_results("SELECT * FROM " .$wpdb->prefix.static::$table_name);
	}

	public static function find_limited($limit, $offset) {
		global $wpdb;
		return $wpdb -> get_results("SELECT * FROM " .$wpdb->prefix.static::$table_name . " ORDER by id DESC LIMIT $offset, $limit ");
		//return $wpdb->last_query;
	}

	public static function find_last_id() {
		global $wpdb;
		$obj = $wpdb -> get_row("SELECT id FROM " .$wpdb->prefix.static::$table_name . " ORDER BY id DESC LIMIT 1");
		return $obj -> id;
	}

	public static function find_by_id($id) {
		global $wpdb;
		$rr=  $wpdb -> get_row("SELECT * FROM " .$wpdb->prefix.static::$table_name . " WHERE id=" . $id);
                return ($rr);
        die();
                
	}

	public static function find_by_parent_id($id) {
		global $wpdb;
		return $wpdb -> get_results("SELECT * FROM " .$wpdb->prefix.static::$table_name . " WHERE parent_id=" . $id . " AND blog_id = " . get_current_blog_id() . " AND " . static::$active_status_condition);
	}

	public static function find_by_field($field, $value) {
		global $wpdb;
		return  $wpdb -> get_results("SELECT * FROM " .$wpdb->prefix.static::$table_name . " WHERE " . $field . "=" . $value );
		// echo $wpdb->last_query;

	}

	public static function find_by_field_limited($field, $value,$limit, $offset) {
		global $wpdb;
		return  $wpdb -> get_results("SELECT * FROM " .$wpdb->prefix.static::$table_name . " WHERE " . $field . "=" . $value. " ORDER by id ASC LIMIT " . $limit . " OFFSET " . $offset);
		// echo $wpdb->last_query;

	}


	public static function find_where($where) {
		global $wpdb;
		return $wpdb -> get_results("SELECT * FROM " .$wpdb->prefix.static::$table_name . " WHERE " . $where . " AND blog_id = " . get_current_blog_id() . " AND " . static::$active_status_condition);
	}
	
	public static function find_like($where,$like) {
		global $wpdb;
		 $wpdb -> get_results("SELECT * FROM " .$wpdb->prefix.static::$table_name . " WHERE " . $where . "  like '%" . $like. "%'");
			echo $wpdb->last_query.'<br>';
	}

	public static function create($post) {
		global $wpdb;
		$result = $wpdb -> insert($wpdb->prefix.static::$table_name, $post);
		//echo $wpdb->last_query.'<br>';
		return $result;

	}

	public static function update($post, $id) {
		global $wpdb;
		return $result = $wpdb -> update($wpdb->prefix.static::$table_name, $post, array('id' => $id));
		//echo $wpdb->last_query.'<br>';
		//return $result;
	}

	public static function delete($id) {
		global $wpdb;
		$result = $wpdb -> query('DELETE FROM ' .$wpdb->prefix.static::$table_name . '
			   WHERE id = ' . $id);
		return $result;

	}
    public static function delete_by_field($field_id,$id) {
            global $wpdb;
            return $result = $wpdb -> query('DELETE FROM ' .$wpdb->prefix.static::$table_name . '
                   WHERE '.$field_id .'='  . $id);
        }
        
	//return with last inserted id
	public function insert($post) {

		global $wpdb;
		$result = $wpdb -> insert($wpdb->prefix.static::$table_name, $post);

		//echo $wpdb->last_query;
		$lastid = $wpdb -> insert_id;
		return $lastid;

	}

	public static function find_by_id_noblogid($id) {
		global $wpdb;
		return $wpdb -> get_row("SELECT * FROM " .$wpdb->prefix.static::$table_name . " WHERE id=" . $id . " AND " . static::$active_status_condition);

	}

	public static function find_all_noblogID() {
		global $wpdb;
		return $wpdb -> get_results("SELECT * FROM " .$wpdb->prefix.static::$table_name . " WHERE  " . static::$active_status_condition);
	}

	public static function find_by_field_noblogID($field, $value) {
		global $wpdb;
		return $wpdb -> get_results("SELECT * FROM " .$wpdb->prefix.static::$table_name . " WHERE " . $field . "=" . $value . " AND " . static::$active_status_condition);

	}
	
	public static function find_count() {
		global $wpdb;
		return $wpdb -> get_var("SELECT COUNT(`id`)  FROM " .$wpdb->prefix.static::$table_name );
		 //return $wpdb ->last_query;
	}
	
	public static function find_distinict($field) {
		global $wpdb;
		return $wpdb -> get_results("SELECT DISTINCT ".$field." FROM " .$wpdb->prefix.static::$table_name . " WHERE ". static::$active_status_condition);
//echo $wpdb->last_query;
	}
	public static function find_by_field_order_by($field,$value,$order) {
		global $wpdb;
		return  $wpdb -> get_results("SELECT * FROM " .$wpdb->prefix.static::$table_name . " WHERE " . $field . "=" . $value . " AND " . static::$active_status_condition ." order by ".$order);
//echo $wpdb->last_query;
	}
	

}
?>