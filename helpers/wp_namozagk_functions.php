<?php

function RunNamozagkPlugin() {


    add_action('admin_menu', array('FormController', 'add_form_page'));
    add_action('admin_menu', array('FormController', 'add_sub_menu_page'));
    add_action('admin_head', array('FormShortCode', 'fb_add_tinymce'));
    add_action('admin_footer', array('FormController', 'get_all_for_shortcode'));
    add_action('admin_post_add_namozagk_form', 'namozagk_add_form');
    //
    add_shortcode('MnbaaNamozagkForm', array('FormShortCode', 'draw_form'));
    //ajax
    add_action('wp_ajax_draw_ajax_ele', array('FormController', 'draw_ajax_ele'));
    add_action('wp_ajax_add_form_style', array('FormController', 'add_form_style'));
    add_action('wp_ajax_post_data', array('FormController', 'post_data'));
    add_action('wp_ajax_nopriv_post_data', 'post_data');
    //

    add_action('admin_enqueue_scripts', "plugin_admin_scripts");
    add_action('wp_enqueue_scripts', "end_user_scripts");
}

function namozagk_add_form() {
    FormController::add_form();
}

function plugin_admin_scripts() {
    $lang = get_bloginfo("language");
    $lang = substr($lang, 0, 2);
    wp_enqueue_style('style', plugins_url('', __FILE__) . '/../views/styles/style-' . $lang . '.css');
    wp_enqueue_script('darg_drop', plugins_url('', __FILE__) . '/../views/js/darg_drop.js');
    wp_enqueue_script('tabs', plugins_url('', __FILE__) . '/../views/js/tabs.js', array('jquery-ui-tabs'));
    wp_enqueue_script('jquery-ui-sortable');
    wp_enqueue_script('jquery-ui-draggable');
    wp_enqueue_script('jquery-ui-droppable');
    wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
    wp_localize_script('darg_drop', 'myAjax', array('ajaxurl' => admin_url('admin-ajax.php')));
}

//
function end_user_scripts() {
    // wp_enqueue_script('require', plugins_url('mnbaa_namozagk/views/js/require.js'),array('jquery', 'json2'));
    // wp_enqueue_script('main', plugins_url('mnbaa_namozagk/views/js/main.js'),array('jquery', 'json2'));
    wp_register_script("user-form-ajax-submit", plugins_url('mnbaa_namozagk/views/js/user-form-ajax-submit.js'), array('jquery', 'json2'));
    wp_localize_script('user-form-ajax-submit', 'frontendAjax', array('ajaxurl' => admin_url('admin-ajax.php')));
    wp_enqueue_script('user-form-ajax-submit');
    
    //custom style file which contain form style
    wp_enqueue_style( 'user_style', plugins_url('../views/styles/user_style.css', __FILE__) );
}
    


?>