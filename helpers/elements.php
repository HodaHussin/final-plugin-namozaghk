<?php

$elements = array(
    'text' => array(
        'tag' => 'input',
        'name'=>'',
        'Label' => __('Text', 'namozaghk'),
        'Placeholder' => 'placeholder',
        'Value' => '',
        'class'=>'',
    ),
    'password' => array(
        'tag' => 'input',
        'Label' => __('Password', 'namozaghk'),
        'Placeholder' => 'placeholder',
        'Value' => '',
        'name'=>'',
        'class'=>'',
    ),
    'textarea' => array(
        'tag' => 'textarea',
        'name'=>'',
        'Label' => __('Textarea', 'namozaghk'),
        'Placeholder' => 'placeholder',
        'Value' => '',
        'class'=>'',
   
    ),
   
    'radio' => array(
        'tag' => 'input',
        'name'=>'',
        'class'=>'',
        'Label' => __('Radio', 'namozaghk'),
        'Options' => array(
            "option_one" => "option_name",
            "option_two" => "option_name",
            "option_three" => "option_name",
        ),
    ),
    'checkbox' => array(
        'tag' => 'input',
        'name'=>'',
        'class'=>'',
        'Label' => __('Checkbox', 'namozaghk'),
        'Value' => '',
    ),
    'select' => array(
        'tag' => 'select',
        'class'=>'',
        'Label' => __('Select', 'namozaghk'),
        'name'=>'',
        'Options' => array(
            "option_one" => "option_name",
            "option_two" => "option_name",
            "option_three" => "option_name",
        ),
    ),
    'file' => array(
        'tag' => 'input',
        'name'=>'',
        'class'=>'',
        'Label' => __('Upload', 'namozaghk'),
        'Placeholder' => 'placeholder',
        'Value' => '',
    ),
    'submit' => array(
        'tag' => 'input',
        'Value' => 'Submit',
        'class'=>'',
    ),
    'label' => array(
        'tag' => 'tag',
        'tagName' => __('label', 'namozaghk'),
        'Text' => 'Title',
        'fontSize' => '14px',
        'class'=>'',
    ),
    /////////////////////html5 elements////////////////////
    'color' => array(
        'name'=>'',
        'tag' => 'input',
        'Label' => __('Color', 'namozaghk'),
        'class'=>'',
   
    ),
    'mail' => array(
        'tag' => 'input',
        'name'=>'',
        'Label' => __('Email', 'namozaghk'),
        'Placeholder' => 'placeholder',
        'Value' => '',
        'class'=>'',
    ),
    'url' => array(
        'tag' => 'input',
        'name'=>'',
        'Label' => __('URL', 'namozaghk'),
        'Placeholder' => 'placeholder',
        'Value' => '',
        'class'=>'',
    ),
    'range' => array(
        'tag' => 'input',
        'name'=>'',
        'Label' => __('Range', 'namozaghk'),
        'Placeholder' => 'placeholder',
        'Value' => '',
        'class'=>'',
   
    ),
    'number' => array(
        'tag' => 'input',
        'name'=>'',
        'Label' => __('Number', 'namozaghk'),
        'Placeholder' => 'placeholder',
        'Value' => '',
        'min' => '5',
        'max' => '4',
        'class'=>'',
   
    ),
    'time' => array(
        'tag' => 'input',
        'name'=>'',
        'Label' => __('Time', 'namozaghk'),
        'Placeholder' => 'placeholder',
        'Value' => '',
        'class'=>'',
    
    ),
    'captacha' => array(
        'tag' => 'captacha',
        'Label' => __('capatch', 'namozaghk'),
        'class'=>'',
    ),
);
$user_options = array(
    'send_email' => '',
    'redirct_url' => array(
        'url' => '',
        'method' => 'POST'
    )
);
?>
