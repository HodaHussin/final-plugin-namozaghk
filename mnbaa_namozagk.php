<?php

/* Plugin Name: Mnbaa Namozagk
  /* Author:      Mnbaa
 * Author URI:  http://bueltge.de
 * License:     GPLv2
 * License URI: ./assets/license.txt
 * Text Domain: namozaghk
 * Domain Path: /languages
 * Network:     false
 */

//load models
include (plugin_dir_path(__FILE__) . 'models/namozagk_db.php');
include (plugin_dir_path(__FILE__) . 'models/form.php');
include (plugin_dir_path(__FILE__) . 'models/element.php');

//load helper
include (plugin_dir_path(__FILE__) . 'helpers/namozagk_functions.php');
include (plugin_dir_path(__FILE__) . 'helpers/wp_namozagk_functions.php');
include (plugin_dir_path(__FILE__) . 'helpers/elements.php');

//load controllers
include (plugin_dir_path(__FILE__) . 'controllers/form_shortcode.php');
include (plugin_dir_path(__FILE__) . 'controllers/forms.php');

//
load_plugin_textdomain('namozaghk', false, dirname(plugin_basename(__FILE__)) . '/languages/');

//filters

register_activation_hook(__FILE__, 'mnbaa_namozagk_activate');

function mnbaa_namozagk_activate() {

    require_once (ABSPATH . 'wp-admin/includes/upgrade.php');
    //
    global $jal_db_version, $wpdb;
    $jal_db_version = "1.0";

    $forms_table_name = $wpdb->prefix . "mnbaa_forms";
    $elements_table_name = $wpdb->prefix . "mnbaa_elements";

    $sql = "CREATE TABLE $forms_table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            name tinytext NOT NULL ,
            class varchar(265) NOT NULL ,
            user_id INT NOT NULL,
            elements TEXT NOT NULL,
            user_options TEXT NOT NULL,
            direction varchar(265) NOT NULL DEFAULT 'ltr',
            status enum ('1','0') DEFAULT 1,
            UNIQUE KEY id (id)
    )ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";

    $sql2 = "CREATE TABLE $elements_table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            form_id  INT NOT NULL ,
            data TEXT NOT NULL,
            UNIQUE KEY id (id)
    )ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";

    dbDelta($sql);
    dbDelta($sql2);
    //attrbutes tables

    add_option("jal_db_version", $jal_db_version);
}

RunNamozagkPlugin();
?>
